use std::fs::File;
use std::io::Write;
use std::thread;
use std::fmt;
use std::time::Duration;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::sync::{
    Arc, 
    atomic::{AtomicBool, Ordering}
};
use std::sync::mpsc::{Sender, Receiver, channel};
use serde::{Serialize, Deserialize};
use signal_hook::flag as sigaction;

pub enum Error {
    Generic(String),
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Generic(format!("{}", e))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            Error::Generic(s) => write!(f, "{}", s),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Counter {
    counter: usize,
}

impl From<usize> for Counter {
    fn from(counter: usize) -> Self {
        Self { counter }
    }
}

#[derive(Serialize, Deserialize, Default)]
pub struct Producer {
    routing_key: String,
    payload: Vec<HashMap<String, usize>>,
}

impl TryFrom<&String> for Producer {
    type Error = Error;
    fn try_from(s: &String) -> Result<Self, Error> {
        serde_json::from_str(s).map_err(|e| Error::Generic(format!("{}", e)))
    }
}

#[derive(Serialize, Deserialize)]
pub struct Datas {
    map: HashMap<String, Vec<Producer>>,
}

impl Datas {
    fn new() -> Self {
        Self {
            map: HashMap::new()
        }
    }

    fn push(&mut self, p: Producer) {
        if let Some(key) = self.map.get_mut(&p.routing_key) {
            key.push(p);
        }
        else {
            let mut v = Vec::new();
            let key = p.routing_key.clone();
            v.push(p);
            self.map.insert(key, v);
        }
    }

    fn save(&mut self) -> Result<(), Error> {
        let mut f = File::create("epic.json")?;
        f.write(serde_json::to_string(&self).unwrap().as_bytes())?;
        Ok(())
    }
}

impl Drop for Datas {
    fn drop(&mut self) {
        self.save().unwrap_or_else(|e| eprintln!("{}", e));
    }
}

fn setup_signals() -> Arc<AtomicBool> {
    let terminate = Arc::new(AtomicBool::new(false));
    let _ = sigaction::register(libc::SIGINT, terminate.clone());
    let _ = sigaction::register(libc::SIGTERM, terminate.clone());

    terminate
}

fn main() {
    let signals = setup_signals();

    let (tx, rx): (Sender<String>, Receiver<String>) = channel();
    let sig = signals.clone();
    let handle = thread::spawn(move|| {
        let mut datas = Datas::new();
        while !sig.load(Ordering::Relaxed) {
            match rx.try_recv() {
                Ok(data) => {
                    match Producer::try_from(&data) {
                        Ok(p) => datas.push(p),
                        Err(e) => eprintln!("{}", e)
                    }
                }
                Err(_) => {
                    // not an error
                }
            };
            std::thread::sleep(Duration::from_millis(500));
        }
        println!("Terminated Thread");
    });

    let mut counter = 0;
    while !signals.load(Ordering::Relaxed) {
        std::thread::sleep(Duration::from_millis(100));
        
        // ugly just as we want it
        let mut s = String::from(r#"{"routing_key":"foo","payload":[{"counter":"#);
        s+= &format!("{}}}]}}", counter);
        println!("{}", s);
        counter += 1;
        tx.send(s).unwrap_or(());
    }

    handle.join().unwrap_or(());

    println!("Terminated");
}
